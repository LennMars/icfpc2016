from util.common import *
from plot_problem import *
from fold import *
import re

init_solution_positions = [p00, p10, p11, p01]
init_result_positions = [p00, p10, p11, p01]
init_facets = [[0, 1, 2, 3]]
init = {'sol_positions': init_solution_positions,
        'res_positions': init_result_positions,
        'facets': init_facets}
def apply_commands(commands, state):
    for command in commands:
        state = fold(command, state)
    return state

def make_pythagoras(m, n):
    return (m*m-n*n, 2*m*n, m*m + n*n)

def count_character(str):
    str=re.sub(r'\s|\n','',str)
    return len(str)

if __name__ == '__main__':
    commands = []
    commands.append(str_to_segment('0,1 1,0'))
    commands.append(str_to_segment('1/2,1/2 0,0'))
    commands.append(str_to_segment('0,1/2 1/2,1/2'))
    commands.append(str_to_segment('0,1/2 1/4,1/4'))
    commands.append(str_to_segment('0,1/4 1/4,1/4'))
    commands.append(str_to_segment('0,1/4 1/8,1/8'))
    commands.append(str_to_segment('1/12,1/12 0,1/6'))

    result = apply_commands(commands, init)
    sol = origami_to_solution(result)
    draw_problem(origami_to_problem(result), "check.png")
    print(sol)
    print(count_character(sol))


    exit()
