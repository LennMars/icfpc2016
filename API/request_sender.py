import sys
import requests
import json
import time

class RequestSender(object):
    def __init__(self):
        self._api_key = '50-c9423ac3e5956e798438d80146814cd7'

    # for debug
    #GET
    # <= 1000 requests/hour
    # no parameter
    def hello_world(self):
        endpoint = 'http://2016sv.icfpcontest.org/api/hello'
        return self._get_request(endpoint)

    #Looks up a blob (the content of something) by its hash. The hash can be: problem_spec_hash, solution_spec_hash, snapshot_hash, etc.
    #GET
    # <= 	1000 requests / hour
    # hash	Hash of a blob.
    def blob_lookup(self,hash):
        #Endpoint	http://2016sv.icfpcontest.org/api/blob/[hash]
        endpoint = 'http://2016sv.icfpcontest.org/api/blob/' + hash
        print(endpoint)
        return self._get_request(endpoint)

    #Returns the list of contest information snapshots. Snapshots are computed only once per hour. Use blob lookup API to retrieve actual snapshots by snapshot_hash.
    #GET
    #	1000 requests / hour
    #No parameter
    def contest_status(self):
        endpoint = 'http://2016sv.icfpcontest.org/api/snapshot/list'
        return self._get_request(endpoint)

    #Submits a problem, so that the problem is scheduled to be published at the designated timestamp. If you submit multiple problems with same timestamp, the latest submission will be used.
    #	POST
    # 	1000 requests / hour (the number of requests is the sum of the problem and solution submissions)
    #  solution_spec	The specification of a valid and normalized solution that produces the problem you want to submit.
    #  publish_time      Timestamp of when you want the problem to be published.
    #                   1470441600 (2016-08-06 00:00:00 UTC)  , 1470445200 (2016-08-06 01:00:00 UTC) ...  1470603600 (2016-08-07 21:00:00 UTC)
    def problem_submission(self,solution_spec,publish_time):
        endopoint = 'http://2016sv.icfpcontest.org/api/problem/submit'
        files = {'solution_spec': open(solution_spec,'rb')}
        form = {
            'publish_time': publish_time
        }
        return self._post_request(endopoint,files,form)

    #Submits a solution to the specified problem.
    # POST
    # 1000 requests / hour (the nubmer of requests is the sum of those of the problem and solution submissions)
    # problem_id	The ID of the problem you are solving.
    # solution_spec	The specification of your solution to the problem.
    def solution_submission(self,probrem_id,solution_spec):
        endpoint = 'http://2016sv.icfpcontest.org/api/solution/submit'
        files = {'solution_spec':open(solution_spec,'rb')}
        form = {
            'problem_id':probrem_id
        }

        return self._post_request(endpoint,files,form)

    def _get_request(self,uri):

        headers = {'Expect':'','X-API-Key':self._api_key}
        resp = requests.get(url=uri,headers=headers)

        return resp

    def _post_request(self,uri,files,form):
        headers = {'Expect': '', 'X-API-Key': self._api_key}
        resp = requests.post(url=uri,files=files,data=form,headers=headers)

        return resp

if __name__ == '__main__':
    r_sender = RequestSender()
    hash = ''

    hello_resp = r_sender.hello_world()
    if(hello_resp.status_code == 200):
        hello = hello_resp.json()
        print(hello['ok'])
        print(hello['greeting'])
    time.sleep(1)
    print("===")
    content_status_resp = r_sender.contest_status()
    if(content_status_resp.status_code == 200):
        content_status = content_status_resp.json()
        print('snapshots #',end="")
        print(len(content_status['snapshots']))
        for snapshot in content_status['snapshots']:
            print(snapshot)
            hash = snapshot['snapshot_hash']
    time.sleep(1)
    print("===")

    blob_resp = r_sender.blob_lookup(hash)
    if(blob_resp.status_code == 200):
        blob = blob_resp.json()
        print(blob)
    time.sleep(1)
    print("===")

    probrem_resp = r_sender.problem_submission('D:/mokeo/repos/icfpc2016/answer/1-1','1470603600')
    print(probrem_resp)
    if(probrem_resp.status_code == 200):
        probrem = probrem_resp.json()
        print(probrem)
    time.sleep(1)
    print("===")

    solution_resp = r_sender.solution_submission(probrem_id='1',solution_spec='D:/mokeo/repos/icfpc2016/answer/1-1')
    print(solution_resp)
    if(solution_resp.status_code ==200):
        solution = solution_resp.json()
        print (solution)