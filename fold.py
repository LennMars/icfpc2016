from util.common import *

p00 = (kZero, kZero)
p10 = (kOne, kZero)
p11 = (kOne, kOne)
p01 = (kZero, kOne)

def remove_duplicates(origami):
    print('--- duplication remove start ---')
    print('sol_positions before dup remove', list(map(point_to_str, origami['sol_positions'])))
    #print('res_positions before dup remove', list(map(point_to_str, origami['res_positions'])))
    n = len(origami['sol_positions'])
    assert(n == len(origami['res_positions']))
    ps = list(zip(range(n), origami['sol_positions'], origami['res_positions']))
    dup_map = {}
    for i in range(n):
        if i in dup_map:
            continue
        for j in range(i + 1, n):
            if j in dup_map:
                continue
            if equalpos(ps[i][1], ps[j][1]):
                dup_map[j] = i

    print('dup_map:', dup_map)
    ps = list(filter(lambda p: not p[0] in dup_map, ps))
    print('sol_positions after dup remove', list(map(lambda p: point_to_str(p[1]), ps)))
    new_n = len(ps)

    renumber_map = {}
    for i in range(new_n):
        renumber_map[ps[i][0]] = i
    print('renumber_map:', renumber_map)
    new_sol_positions = list(map(lambda p: p[1], ps))
    new_res_positions = list(map(lambda p: p[2], ps))
    new_facets = []
    for facet in origami['facets']:
        def dedup_and_renumber(i):
            if i in dup_map:
                j = dup_map[i]
            else:
                j = i
            return renumber_map[j]
        new_facet = list(map(dedup_and_renumber, facet))
        new_facets.append(remove_consecutive_duplicates(new_facet))
    return {'sol_positions': new_sol_positions,
            'res_positions': new_res_positions,
            'facets': new_facets}

def linear_to_segment(origami, a, b, reverse=False):  # get fold segment along y = a x + b.
    positions = origami['res_positions']
    positions = sorted(positions, key=lambda p: number_to_float(p[0]))  # sort by x-axis.
    x0 = positions[0][0]
    x1 = positions[-1][0]
    y0 = plus(mul(a, x0), b)
    y1 = plus(mul(a, x1), b)
    if reverse:
        return ((x1, y1), (x0, y0))
    else:
        return ((x0, y0), (x1, y1))

def vertical_to_segment(origami, c, reverse=False):  # get fold segment along x = c.
    positions = origami['res_positions']
    positions = sorted(positions, key=lambda p: number_to_float(p[1]))  # sort by y-axis.
    y0 = positions[0][1]
    y1 = positions[-1][1]
    if reverse:
        return ((c, y1), (c, y0))
    else:
        return ((c, y0), (c, y1))

def origami_to_problem(origami):
    polygons = []
    segments = []
    for facet in origami['facets']:
        polygon = list(map(lambda i: origami['res_positions'][i], facet))
        polygons.append(polygon)
        for pair in facet_to_pairs(facet):
            segment = pair_to_segment(origami['res_positions'], pair)
            segments.append(segment)
    return polygons, segments

def fold_position(segment, p):
    p1, p2 = segment
    assert(nepos(p1, p2))
    x1, y1 = p1
    x2, y2 = p2
    p = reduce_pos(p)
    x, y = p
    xd = sub(x2, x1)
    yd = sub(y2, y1)
    wa = mul(xd, xd)
    wc = mul(yd, yd)
    wb = mul(xd, sub(x1, x))
    wd = mul(yd, sub(y1, y))
    w = mul(kMinusOne, div(plus(wb, wd), plus(wa, wc)))
    w1 = mul(kTwo, sub(kOne, w))
    w2 = mul(kTwo, w)
    fold_x = sub(plus(mul(w1, x1), mul(w2, x2)), x)
    fold_y = sub(plus(mul(w1, y1), mul(w2, y2)), y)
    folded = (fold_x, fold_y)
    folded_reduced = reduce_pos(folded)
    print('folded %s -> %s' % (point_to_str(p), point_to_str(folded_reduced)))
    return folded_reduced

def fold_res_positions(fold_segment, res_positions):
    print('--- result position folding start ---')
    def fold_position_if_left(index_and_position):
        i, pos = index_and_position
        if is_left_of_segment(fold_segment, pos):
            folded = fold_position(fold_segment, pos)
            print('left of segment found, point %d: %s -> %s' % (i, point_to_str(pos), point_to_str(folded)))
            return folded
        else:
            return pos
    return list(map(fold_position_if_left, enumerate(res_positions)))

def fold(fold_segment, origami):
    print('----- fold start -----')
    print('fold segment (s1, s2):', segment_to_str(fold_segment))
    p1, p2 = fold_segment
    assert(nepos(p1, p2))
    all_pairs = flatten(map(facet_to_pairs, origami['facets']))
    print('all edge pair in solution facets', all_pairs)
    weights = []
    for pair in set(all_pairs):
        assert(pair[0] != pair[1])
        res_segment = pair_to_segment(origami['res_positions'], pair)
        w = find_on_segment(fold_segment, res_segment, end_inclusive=True)
        if w is not None:
            w = reduce_num(w)
            weights.append((pair, w))
            print('fold segment hits on edge (%d,%d) by weight %s' % (pair[0], pair[1], number_to_str(w)))
    new_sol_positions = []
    new_res_positions = []
    new_facets = []
    old_facets = []
    position_len = len(origami['sol_positions'])
    new_position_index = [position_len]
    for facet in origami['facets']:
        pairs = facet_to_pairs(facet)
        weights_in_facet = list(filter(lambda pw: pw[0] in pairs, weights))
        n = len(weights_in_facet)
        if n == 0:
            continue
        print('%d weights_in_facet found' % n, list(map(lambda pw: '%s, %s' % (pw[0], number_to_str(pw[1])), weights_in_facet)))
        all_pairs = get_all_pairs(weights_in_facet)
        def is_weight_pair_to_different_point(weight1, weight2):
            p1, w1 = weight1
            p2, w2 = weight2
            sol_segment1 = pair_to_segment(origami['sol_positions'], p1)
            sol_segment2 = pair_to_segment(origami['sol_positions'], p2)
            position1 = get_weighted_position_in_segment(sol_segment1, w1)
            position2 = get_weighted_position_in_segment(sol_segment2, w2)
            return nepos(position1, position2)
        all_pairs = list(filter(lambda ww: is_weight_pair_to_different_point(*ww), all_pairs))
        for weight1, weight2 in [all_pairs[0]]:
            print('--- divide facet %s ---' % facet)
            print('pairs in this facet:', pairs)
            def add(weight):
                old_facets.append(facet)
                pair, w = weight
                print('select for edge (%d,%d) by weight %s' % (pair[0], pair[1], number_to_str(w)))
                if equal(w, kZero):
                    point = pair[0]
                    print('weight = 0, use point', point)
                    new_sol_position = origami['sol_positions'][point]
                elif equal(w, kOne):
                    point = pair[1]
                    print('weight = 1, use point', point)
                    new_sol_position = origami['sol_positions'][point]
                else:
                    sol_segment = pair_to_segment(origami['sol_positions'], pair)
                    res_segment = pair_to_segment(origami['res_positions'], pair)
                    new_sol_position = get_weighted_position_in_segment(sol_segment, w)
                    new_res_position = get_weighted_position_in_segment(res_segment, w)
                    new_sol_positions.append(new_sol_position)
                    new_res_positions.append(new_res_position)
                    point = new_position_index[0]
                    print(' 0 < weight < 1, add new point', point)
                    print('new point solution position:', point_to_str(new_sol_position))
                    print('new point result position:', point_to_str(new_res_position))
                    new_position_index[0] += 1
                return pair, point, new_sol_position
            print('- select or add point for s1 -')
            pair1, point1, position1 = add(weight1)
            print('- select or add point for s2 -')
            pair2, point2, position2 = add(weight2)
            if equalpos(position1, position2):
                print('tried to add segment between identical positions, skip')
                continue
            print('- point selection ended -')
            print('facet edges revisited', pairs)
            i1 = get_index(pairs, pair1)
            i2 = get_index(pairs, pair2)
            assert(i1 is not None)
            assert(i2 is not None)
            print('point %d is added between edge (%d,%d)' % (point1, pairs[i1][0], pairs[i1][1]))
            print('point %d is added between edge (%d,%d)' % (point2, pairs[i2][0], pairs[i2][1]))
            if i1 < i2:
                new_facet1 = [point1, point2]
                new_facet2 = [point2, point1]
                #print('case 1 new facet', new_facet1, new_facet2)
                for pair in pairs[i2 :]:
                    new_facet1.append(pair[1])
                    #print('new_facet1.append', pair[1])
                for pair in pairs[: i1]:
                    new_facet1.append(pair[1])
                    #print('new_facet1.append', pair[1])
                for pair in pairs[i1 : i2]:
                    new_facet2.append(pair[1])
                    #print('new_facet2.append', pair[1])
            elif i2 < i1:
                new_facet1 = [point2, point1]
                new_facet2 = [point1, point2]
                #print('case 2 new facet', new_facet1, new_facet2)
                for pair in pairs[i1 :]:
                    new_facet1.append(pair[1])
                    #print('new_facet1.append', pair[1])
                for pair in pairs[: i2]:
                    new_facet1.append(pair[1])
                    #print('new_facet1.append', pair[1])
                for pair in pairs[i2 : i1]:
                    new_facet2.append(pair[1])
                    #print('new_facet2.append', pair[1])
            else:
                assert(False)
            #print('new facetA', new_facet1, new_facet2)
            def try_adding_facet(new_facet):
                print('try adding facet', new_facet)
                new_facet = remove_consecutive_duplicates(new_facet)
                while new_facet[-1] == new_facet[0]:
                    new_facet = new_facet[: -1]
                if len(new_facet) <= 2:
                    print('new facet length: %d, skip' % len(new_facet))
                else:
                    new_facets.append(new_facet)
            try_adding_facet(new_facet1)
            try_adding_facet(new_facet2)
        print('new_facets', new_facets)

    sol_positions = origami['sol_positions'] + new_sol_positions
    res_positions = origami['res_positions'] + new_res_positions
    res_positions = fold_res_positions((p1, p2), res_positions)
    facets = list(filter(lambda facet: not facet in old_facets, origami['facets'])) + new_facets
    new_origami = {'sol_positions': sol_positions,
                   'res_positions': res_positions,
                   'facets': facets}
    print('result facets:', new_origami['facets'])
    new_origami_remove_dup = remove_duplicates(new_origami)
    print('result facets after dedup:', new_origami_remove_dup['facets'])
    return new_origami_remove_dup


def origami_to_solution(origami):
    sol_positions = origami["sol_positions"]
    res_positions = origami["res_positions"]
    facets = origami["facets"]
    return sol_to_str(sol_positions)+"\n"+facets_to_str(facets)+"\n"+res_to_str(res_positions)

def sol_to_str(sol_positions):
    n = len(sol_positions)
    posstr_lst = [point_to_str(p) for p in sol_positions]
    output = str(n)+"\n"+"\n".join(posstr_lst)
    return output

def res_to_str(res_positions):
    resstr_lst = [point_to_str(p) for p in res_positions]
    output = "\n".join(resstr_lst)
    return output

def facets_to_str(facets):
    def one_facet_to_str(facet):
        num = len(facet)
        s = (str(num) + " ") + " ".join([str(item) for item in facet])
        return s

    n = len(facets)
    facetstr_lst = [one_facet_to_str(facet) for facet in facets]
    output = str(n)+"\n"+"\n".join(facetstr_lst)
    return output
