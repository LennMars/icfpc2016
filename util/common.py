kZero = (0, 1)
kOne = (1, 1)
kMinusOne = (-1, 1)
kTwo = (2, 1)
pZero = (kZero, kZero)
from fractions import Fraction

def euclidean(m, n):
    if n > m:
        return euclidean(n, m)
    if n == 0:
        return m
    else:
        return euclidean(n, m % n)

def lcm(m,n):
    output = int(Fraction(m,euclidean(m,n))*n)
    return output

def flatten(xss):
    ys = []
    for xs in xss:
        ys.extend(xs)
    return ys

def str_to_number(s):
    ss = s.split('/')
    if len(ss) == 1:
        return (int(ss[0]), 1)
    elif len(ss) == 2:
        return reduce_num((int(ss[0]), int(ss[1])))
    else:
        assert(False)

def str_to_point(s):
    ss = s.split(',')
    return (str_to_number(ss[0]), str_to_number(ss[1]))

def str_to_segment(s):
    ss = s.split(' ')
    return (str_to_point(ss[0]), str_to_point(ss[1]))

def point_to_str(p):
    ((xn, xd), (yn, yd)) = reduce_pos(p)
    if xn == 0:
        xs = '0'
    elif xd == 1:
        xs = str(xn)
    else:
        xs = '%d/%d' % (xn, xd)
    if yn == 0:
        ys = '0'
    elif yd == 1:
        ys = str(yn)
    else:
        ys = '%d/%d' % (yn, yd)
    return '%s,%s' % (xs, ys)

def read_problem(fp):
    polygons = []
    segments = []
    is_polygon_header = False
    is_segment_header = False
    for i, s in enumerate(fp):
        if i == 0:
            num_polygons = int(s)
            is_polygon_header = True
        elif num_polygons > 0 and is_polygon_header:
            n = int(s)  # Number of segments to read.
            is_polygon_header = False
            polygon = []
        elif num_polygons > 0:
            polygon.append(str_to_point(s))
            n -= 1
            if n == 0:
                polygons.append(polygon)
                num_polygons -= 1
                if num_polygons == 0:
                    is_segment_header = True
                else:
                    is_polygon_header = True
        elif is_segment_header:
            n = int(s)
            is_segment_header = False
        else:
            segments.append(str_to_segment(s))
            n -= 1
    return (polygons, segments)

def plus(a, b):
    (an, ad) = a
    (bn, bd) = b
    assert(ad * bd != 0)
    return reduce_num((an * bd + bn * ad, ad * bd))

def pluspos(p1, p2):
    return (plus(p1[0], p2[0]), plus(p1[1], p2[1]))

def sub(a, b):
    (an, ad) = a
    (bn, bd) = b
    assert(ad * bd != 0)
    return reduce_num((an * bd - bn * ad, ad * bd))

def subpos(p1, p2):
    return (sub(p1[0], p2[0]), sub(p1[1], p2[1]))

def div(a, b):
    (an, ad) = a
    (bn, bd) = b
    assert(ad * bd * bn != 0)
    return reduce_num((an * bd, ad * bn))

def mul(a, b):
    (an, ad) = a
    (bn, bd) = b
    assert(ad * bd != 0)
    return reduce_num((an * bn, ad * bd))

def equal(a, b):
    (an, ad) = a
    (bn, bd) = b
    assert(ad * bd != 0)
    return an * bd == bn * ad

def equalpos(p1, p2):
    return equal(p1[0], p2[0]) and equal(p1[1], p2[1])

def ne(a, b):  # a != b
    return not equal(a, b)

def nepos(p1, p2):
    return not equalpos(p1, p2)

def le(a, b):  # a <= b
    (an, ad) = a
    (bn, bd) = b
    assert(ad * bd != 0)
    if equal(a, kZero):
        assert(bd != 0)
        return 0 <= bn * bd
    else:
        return le(kZero, sub(b, a))

def ge(a, b):  # a >= b
    return le(b, a)

def lt(a, b):
    return le(a, b) and ne(a, b)

def gt(a, b):
    return lt(b, a)

def dot(p1, p2):
    p1x, p1y = p1
    p2x, p2y = p2
    return plus(mul(p1x, p2x), mul(p1y, p2y))

def cross(p1, p2):
    p1x, p1y = p1
    p2x, p2y = p2
    return sub(mul(p1x, p2y), mul(p2x, p1y))

def ccw(p1, p2, p3):
    p2 = subpos(p2, p1)
    p3 = subpos(p3, p1)
    cr = cross(p2, p3)
    if gt(cr, kZero):
        return 1
    elif lt(cr, kZero):
        return -1
    elif lt(dot(p2, p3), kZero):
        return 2
    elif lt(dot(p2, p2), dot(p3, p3)):
        return -2
    else:
        return 0

def get_convex_hull(ps):
    print('psA', ps)
    ps.sort(key=lambda p: number_to_float(p[0]))
    print('psB', ps)
    n = len(ps)
    ch = [pZero] * (2 * n)
    k = 0
    for i in range(n):
        while k >= 2 and ccw(ch[k - 2], ch[k - 1], ps[i]) <= 0:
            k -= 1
        ch[k] = ps[i]
        k += 1
    t = k + 1
    for i in range(n - 2, -1, -1):
        while k >= t and ccw(ch[k - 2], ch[k - 1], ps[i]) <= 0:
            k -= 1
        ch[k] = ps[i]
        k += 1
    return ch[: k]

def reduce_num(a):
    (an, ad) = a
    assert(ad != 0)
    if an == 0:
        return (0, 1)
    else:
        ana = abs(an)
        ada = abs(ad)
        m = euclidean(ana, ada)
        if an * ad > 0:
            return (ana // m, ada // m)
        else:
            return (- ana // m, ada // m)

def reduce_pos(p):
    x, y = p
    return (reduce_num(x), reduce_num(y))

def mulpos(a, p):
    x, y = p
    return (mul(a, x), mul(a, y))

def get_all_pairs(xs):
    pairs = []
    n = len(xs)
    for i in range(n):
        for j in range(i + 1, n):
            pairs.append((xs[i], xs[j]))
    return pairs

def is_left_of_segment(segment, position):
    p1, p2 = segment
    assert(nepos(p1, p2))
    return gt(cross(subpos(p2, p1), subpos(position, p1)), kZero)

def facet_to_pairs(facet):
    pairs = []
    for i in range(len(facet)):
        j1 = facet[i]
        if i == len(facet) - 1:
            j2 = facet[0]
        else:
            j2 = facet[i + 1]
        pairs.append((j1, j2))
    return pairs

def pair_to_segment(positions, pair):
    return (positions[pair[0]], positions[pair[1]])

def find_on_segment(fold_segment, res_segment, end_inclusive=True):
    # fold: crossing line
    # res: crossed line (crossing position weight is calculated)
    assert(nepos(fold_segment[0], fold_segment[1]))
    assert(nepos(res_segment[0], res_segment[1]))
    (f1x, f1y), (f2x, f2y) = fold_segment
    (r1x, r1y), (r2x, r2y) = res_segment
    def s(x, y):
        sa = mul(sub(r1x, r2x), sub(y, r1y))
        sb = mul(sub(r1y, r2y), sub(r1x, x))
        return plus(sa, sb)
    def t(x, y):
        ta = mul(sub(f1x, f2x), sub(y, f1y))
        tb = mul(sub(f1y, f2y), sub(f1x, x))
        return plus(ta, tb)
    s1 = s(f1x, f1y)
    s2 = s(f2x, f2y)
    if le(mul(s1, s2), kZero):  # fold_segment intersects with (infinite line by) res_segment (inclusive).
        t1 = t(r1x, r1y)
        t2 = t(r2x, r2y)
        t12 = mul(t1, t2)
        if le(t12, kZero):  # weight = 0, 1 is inclusive.
            if not end_inclusive and (equal(t2, kZero) or equal(t1, kZero)):
                return None
            if equal(t1, kZero) and equal(t2, kZero):  # On the same line
                return None
            weight = div(t1, sub(t1, t2))
            assert(le(kZero, weight) and le(weight, kOne))
            return weight
        else:  # res_segment does not intersect with (infinite line by) fold_segment.
            return None
    else:
        return None

def get_weighted_position_in_segment(segment, weight):
    x = plus(mul(sub(kOne, weight), segment[0][0]), mul(weight, segment[1][0]))
    y = plus(mul(sub(kOne, weight), segment[0][1]), mul(weight, segment[1][1]))
    return reduce_pos((x, y))

def get_index(ys, x):
    for i, y in enumerate(ys):
        if x == y:
            return i
    return None

def get_area(polygon):
    acc = (0, 1)
    for i in range(len(polygon)):
        p1 = polygon[i]
        if i == len(polygon) - 1:
            p2 = polygon[0]
        else:
            p2 = polygon[i + 1]
        acc = plus(acc, cross(p1, p2))
    return div(acc, (2, 1))

def number_to_float(a):
    n, d = a
    return float(n) / float(d)

def number_to_str(a):
    n, d = a
    return '%d/%d' % (n, d)

def point_to_coord(p):
    xcoord = number_to_float(p[0])
    ycoord = number_to_float(p[1])
    return (xcoord, ycoord)

def segment_to_str(segment):
    p1, p2 = segment
    return '%s %s' % (point_to_str(p1), point_to_str(p2))

def print_problem(problem, fp):
    polygons, segments = problem
    fp.write('%d\n' % len(polygons))
    for polygon in polygons:
        fp.write('%d\n' % len(polygon))
        for p in polygon:
            fp.write('%s\n' % point_to_str(p))
    fp.write('%d\n' % len(segments))
    for p1, p2 in segments:
        fp.write('%s %s\n' % (point_to_str(p1), point_to_str(p2)))

def remove_consecutive_duplicates(xs, p=(lambda xy: xy[0] == xy[1])):
    if xs == []:
        return []
    else:
        last = xs[0]
        ys = [last]
        for i in range(1, len(xs)):
            x = xs[i]
            if p((x, last)):#x == last:
                continue
            else:
                ys.append(x)
                last = x
        if xs != ys:
            print('remove_consecutive_duplicates: %s -> %s' % (xs, ys))
        return ys
