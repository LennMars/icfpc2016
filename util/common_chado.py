import re
import sys
from util.common import *
from fractions import Fraction
from math import sqrt
from collections import defaultdict

def max_rational(r_lst):
    max_r = max([Fraction(r[0], r[1]) for r in r_lst])
    return (max_r.numerator, max_r.denominator)

def min_rational(r_lst):
    min_r = min([Fraction(r[0], r[1]) for r in r_lst])
    return (min_r.numerator, min_r.denominator)

def min_x(point_lst):
    return min_rational([p[0] for p in point_lst])

def max_x(point_lst):
    return max_rational([p[0] for p in point_lst])

def min_y(point_lst):
    return min_rational([p[1] for p in point_lst])

def max_y(point_lst):
    return max_rational([p[1] for p in point_lst])

#sqrt for big int
def mysqrt(sq):
    lower = 1
    upper = sq
    while upper - lower > 100:
        middle = (upper+lower)//2
        if middle*middle < sq:
            lower = middle
        else:
            upper = middle
    for i in range(lower, upper + 1):
        if i*i == sq:
            return i

def is_squarenumber(n):
    n_root = mysqrt(n)
    if n_root is None:
        return False
    return n_root*n_root == n

def get_pythagoras(p1, p2):
    p_dif = subpos(p2, p1)
    dx = p_dif[0]
    dy = p_dif[1]
    if dx[0]*dx[1]*dy[0]*dy[1] == 0:
        return (1,0,1)
    n_lcm = lcm(dx[1], dy[1])
    a = int(Fraction(dx[0],dx[1])*n_lcm)
    b = int(Fraction(dy[0],dy[1])*n_lcm)
    c_square = a*a + b*b
    if is_squarenumber(c_square):
        return (a, b, mysqrt(c_square))
    return None

def normalize_pythagoras(tup):
    a, b, c = tup
    if a < 0:
        return (b, -a, c)
    return tup

def estimate_pythagoras(lst_edge):
    if len(lst_edge)==0:
        return None
    lst_p = [get_pythagoras(p1, p2) for p1, p2 in lst_edge if get_pythagoras(p1, p2)]
    lst_p = [normalize_pythagoras(pythagoras) for pythagoras in lst_p]
    counter = defaultdict(int)
    for pythagoras in lst_p:
        counter[pythagoras] += 1
    max_freq = 0
    max_pythagoras = None
    for key, value in counter.items():
        if value > max_freq:
            max_freq = value
            max_pythagoras = key
    return max_pythagoras

def rotate(p_target, p_anchor, pythagoras):
    cos = (pythagoras[0], pythagoras[2])
    sin = (pythagoras[1], pythagoras[2])
    p_dif = subpos(p_target, p_anchor)
    x_dif_rot = sub(mul(p_dif[0],cos), mul(p_dif[1], sin))
    y_dif_rot = plus(mul(p_dif[1],cos), mul(p_dif[0], sin))
    return pluspos((x_dif_rot, y_dif_rot), p_anchor)

def unrotate(p_target, p_anchor, pythagoras):
    return rotate(p_target, p_anchor, (pythagoras[0], -pythagoras[1], pythagoras[2]))


def rotate_all_vertices(p_lst, p_anchor, pythagoras):
    return [rotate(p_target, p_anchor, pythagoras) for p_target in p_lst]

def unrotate_all_vertices(p_lst, p_anchor, pythagoras):
    return [unrotate(p_target, p_anchor, pythagoras) for p_target in p_lst]

def transfer_all_vertices(p_lst, vector):
    return [pluspos(p_target, vector) for p_target in p_lst]

def rotate_all_edges(edge_lst, p_anchor, pythagoras):
    new_edges = []
    for edge in edge_lst:
        v1 = edge[0]
        v2 = edge[1]
        new_edges.append((rotate(v1, p_anchor, pythagoras), rotate(v2, p_anchor, pythagoras)))
    return new_edges

def unrotate_all_edges(edge_lst, p_anchor, pythagoras):
    new_edges = []
    for edge in edge_lst:
        v1 = edge[0]
        v2 = edge[1]
        new_edges.append((unrotate(v1, p_anchor, pythagoras), unrotate(v2, p_anchor, pythagoras)))
    return new_edges

def transfer_all_edges(edge_lst, vector):
    new_edges = []
    for edge in edge_lst:
        v1 = edge[0]
        v2 = edge[1]
        new_edges.append((pluspos(v1, vector), pluspos(v2, vector)))
    return new_edges


