import os.path
import os
from normalize_problem import *

def count_character(str):
    str=re.sub(r'\s|\n','',str)
    return len(str)

if __name__ == '__main__':
    dir_path = "problems"
    answer_path = "answer"
    ext_type = "rectangle2"
    id_lst = os.listdir(dir_path)
    id_lst = [int(item) for item in id_lst]
    id_lst.sort()
    id_lst = [str(item) for item in id_lst]
    input = [line.split(",") for line in open('log_submit_solution.txt')]
    result = {}
    for line in input[1:]:
        result[int(line[0])] = float(line[1])


    output = [["id", "resemblance", "solution_size"]]
    for id in id_lst:
        try:
            f = open(answer_path+"/"+id+"."+ext_type, "r")
            solution = f.read()
            f.close()
            #item = [id, str(result[int(id)]), str(count_character(solution))]
            item = [id,str(count_character(solution))]
        except FileNotFoundError:
            #item = [id, "no solution", "no solution"]
             item = [id, "no solution"]
        output.append(item)

    fw = open("check_submitted_solution_"+ext_type+".txt", "w")
    for item in output:
        fw.write(",".join(item)+"\n")
    fw.close()
    """
    f.write(result)
    f.close()
    """
