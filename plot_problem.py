import re
import sys
import matplotlib as mp
import matplotlib.pyplot as pp
from util.common import *
from split_problem import *

def draw_problem(problem, filename, to_draw_convex_hull=True):
    polygons, segments = problem
    pp.grid(True)
    lims = [0., 0., 1., 1.]  #min_x, min_y, max_x, max_y
    def draw_segment(p1, p2, color, lw=1):
        c1 = point_to_coord(p1)
        c2 = point_to_coord(p2)
        x1, y1 = c1
        x2, y2 = c2
        pp.plot([x1, x2], [y1, y2], color=color, lw=lw)
        lims[0] = min(lims[0], x1, x2)
        lims[1] = min(lims[1], y1, y2)
        lims[2] = max(lims[2], x1, x2)
        lims[3] = max(lims[3], y1, y2)
    if to_draw_convex_hull:
        cross_points, mini_segments = segments_to_mini_segments(segments)
        convex_hull = get_convex_hull(cross_points)
        print('cross_points', cross_points)
        print('convex_hull', convex_hull)
        for i in range(len(convex_hull)):
            p1 = convex_hull[i]
            if i == len(convex_hull) - 1:
                p2 = convex_hull[0]
            else:
                p2 = convex_hull[i + 1]
            draw_segment(p1, p2, 'green', 5)
    for polygon in polygons:
        for i in range(len(polygon)):
            p1 = polygon[i]
            if i == len(polygon) - 1:
                p2 = polygon[0]
            else:
                p2 = polygon[i + 1]
            draw_segment(p1, p2, 'red', 3)
    for p1, p2 in segments:
        draw_segment(p1, p2, 'blue')
    pp.xlim(lims[0] - 0.1, lims[2] + 0.1)
    pp.ylim(lims[1] - 0.1, lims[3] + 0.1)
    pp.savefig(filename)
    pp.clf()

if __name__ == '__main__':
    problem_path = sys.argv[1]
    with open(problem_path) as fp:
        problem = read_problem(fp)
    fig_path = re.sub('\.[^.]+$', '.png', problem_path)
    print('%s -> %s' % (problem_path, fig_path))
    draw_problem(problem, fig_path)
