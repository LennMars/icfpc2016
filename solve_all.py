import os.path
import os
import traceback
from normalize_problem import *

if __name__ == '__main__':
    dir_path = "problems"
    answer_path = "answer"
    lst = os.listdir(dir_path)
    lst = [int(item) for item in lst]
    lst.sort()
    lst = [str(item) for item in lst]
    result = {}
    for line in open('log_submit_solution.txt'):
        print(line)

    fe = open("log_solve_all.txt", "w")
    for problem in lst:
        problem_path =dir_path+"/"+problem
        try:
            rs = RectangleSolver(problem_path)
            result = rs.solve()
        except Exception as e:
            print("error at id:" + problem)
            fe.write("At id " + problem + "\n" + traceback.format_exc() + "\n")
            continue
        f = open(answer_path+"/"+problem+".rectangle2", "w")
        f.write(result)
        f.close()
    fe.close()
