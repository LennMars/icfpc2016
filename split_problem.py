from util.common import *

def segments_to_mini_segments(segments):
    print('--- segments_to_mini_segments start ---')
    mini_segments = []
    cross_points_all = []
    def add_cross_point(p):  # Add point for the set and return index.
        for i, q in enumerate(cross_points_all):
            if equalpos(p, q):
                return i
        cross_points_all.append(p)
        return len(cross_points_all) - 1
    for i, crossed_segment in enumerate(segments):
        weights = []
        for j, crossing_segment in enumerate(segments):
            if i == j:
                continue
            weight = find_on_segment(crossing_segment, crossed_segment, end_inclusive=True)
            if not weight is None:
                weights.append(weight)
        weights.sort(key=number_to_float)
        weights = remove_consecutive_duplicates(weights, p=(lambda xy: equal(xy[0], xy[1])))
        print('cross points for segment %d: %s' % (i, list(map(number_to_str, weights))))
        n = len(weights)
        assert(n >= 2)
        cross_points = list(map(lambda w: get_weighted_position_in_segment(crossed_segment, w), weights))
        for k in range(n - 1):
            cp1 = cross_points[k]
            cp2 = cross_points[k + 1]
            cp1i = add_cross_point(cp1)
            cp2i = add_cross_point(cp2)
            mini_segments.append({'point_index': (cp1i, cp2i), 'original_segment_index': i})
    return cross_points_all, mini_segments
