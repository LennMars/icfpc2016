import re
import sys
import os.path
import matplotlib as mp
import matplotlib.pyplot as pp
from util.common import *
from util.common_chado import *
from fractions import Fraction
from math import sqrt
from collections import defaultdict
from decimal import *
from plot_problem import *
from fold import *
import traceback

class RectangleSolver:
    def __init__(self):
        self.version = "3"

    def set_problem(self, id):
        self.id = id
        problem_path = "problems/" + str(self.id)
        with open(problem_path) as fp:
            problem = read_problem(fp)
        self.vertices = problem[0][0]
        self.edges = problem[1]
        self.normalize_problem()

    def normalize_problem(self):
        self.pythagoras = estimate_pythagoras(self.edges)
        self.print_vertices()
        self.print_edges()
        print("Normalizing begins")
        p_anchor = self.vertices[0]
        self.vertices = unrotate_all_vertices(self.vertices, p_anchor, self.pythagoras)
        self.origin_x = min_x(self.vertices)
        self.origin_y= min_y(self.vertices)
        print(self.pythagoras)
        print(self.origin_x,self.origin_y)
        self.vertices = transfer_all_vertices(self.vertices, ((-self.origin_x[0],self.origin_x[1]), (-self.origin_y[0],self.origin_y[1])))
        self.edges = unrotate_all_edges(self.edges ,p_anchor, self.pythagoras)
        self.edges = transfer_all_edges(self.edges, ((-self.origin_x[0],self.origin_x[1]), (-self.origin_y[0],self.origin_y[1])))
        self.print_vertices()
        self.print_edges()

    def solve(self, id):
        self.set_problem(str(id))
        max_x_loc = max_x(self.vertices)
        max_y_loc = max_y(self.vertices)
        points_x = self.how_to_fold(max_x_loc)
        points_y = self.how_to_fold(max_y_loc)
        print("points_x")
        print(points_x)
        print("points_y")
        print(points_y)
        commands_x = [((r, (1,1)), (r, (0,1))) for r in points_x]
        commands_y = [(((0,1), r), (min_rational([max_x_loc, (1,1)]), r)) for r in points_y]
        self.commands = commands_x + commands_y
        print("commands")

        # fold
        init_solution_positions = [p00, p10, p11, p01]
        init_result_positions = [p00, p10, p11, p01]
        init_facets = [[0, 1, 2, 3]]
        init = {'sol_positions': init_solution_positions,
                'res_positions': init_result_positions,
                'facets': init_facets}

        state = init
        for command in self.commands:
            print()
            print("=======")
            print(command)
            state = fold(command, state)
        res_lst = state["res_positions"]
        state["res_positions"] = self.unnormalize(res_lst)
        self.print_state(state)
        self.print_commands()
        self.output_solution(state)
        return origami_to_solution(state)

    def how_to_fold(self, r):
        if le((1,1), r):
            return []
        count = 0
        while lt(r, (1,2)):
            count += 1
            r = plus(r, r)
            print(r)
        fold_points = [r]
        for i in range(0, count):
            r = div(r, (2,1))
            fold_points.append(r)
        return fold_points

    def unnormalize(self, p_lst):
        origin = (self.origin_x, self.origin_y)
        output = [pluspos(rotate(p, ((0,1), (0,1)), self.pythagoras), origin) for p in p_lst]
        print(output)
        return output

    def print_vertices(self):
        print("vertices")
        print(self.vertices)

    def print_edges(self):
        print("edges")
        print(self.edges)

    def print_commands(self):
        print()
        print("Applied commands are")
        for i, command in enumerate(self.commands):
            print(str(i)+"th command is " + str(command))

    def print_state(self, state):
        print(origami_to_solution(state))

    def output_solution(self, state):
        file_path = "answer/" + self.id + ".rectangle" + self.version
        try:
            f = open(file_path, "w")
            f.write(origami_to_solution(state)+"\n")
            f.close()
            print("succeed to create "+file_path)
        except Exception as e:
            traceback.print_exc()

    def problems_to_solve(self):
        problems_path = "problems"
        problem_lst = os.listdir(problems_path)
        problem_lst = [int(item) for item in problem_lst]
        problem_lst.sort()
        problem_lst = [str(item) for item in problem_lst]
        print("problem_lst_size", len(problem_lst))

        answer_path = "answer"
        answer_lst = os.listdir(answer_path)
        answer_lst = [os.path.splitext(file_path)[0] for file_path in answer_lst if os.path.splitext(file_path)[1] == ".rectangle" + self.version]
        print("answer_lst_size",len(answer_lst))

        problem_lst = [problem for problem in problem_lst if problem not in answer_lst]
        print("final_problem_lst_size ", len(problem_lst))
        return problem_lst

    def solve_updated_problems(self):
        problem_lst = self.problems_to_solve()
        print(len(problem_lst))
        fe = open("log_rectangle_solver.txt", "w")
        for id in problem_lst:
            try:
                self.solve(id)
            except Exception as e:
                print("error at id:" + id)
                fe.write("At id " + id + "\n" + traceback.format_exc() + "\n")
                continue
        fe.close()

if __name__ == '__main__':
    #id = sys.argv[1]
    rs = RectangleSolver()
    #rs.solve("2264")
    rs.solve_updated_problems()
    """
    problem_lst = rs.problems_to_solve()

    print(problem_lst)
    fe = open("log_rectangle_solver.txt", "w")
    for id in problem_lst:
        try:
            rs.solve(id)
        except Exception as e:
            print("error at id:" + id)
            fe.write("At id " + id + "\n" + traceback.format_exc() + "\n")
            continue
    fe.close()
    """
