import re
import sys
import matplotlib as mp
import matplotlib.pyplot as pp
from util.common import *
from fractions import Fraction
from math import sqrt
from collections import defaultdict
from decimal import *
from plot_problem import *
from fold import *

class IrrationalError(Exception):pass
def distance(d):
    #d = (dx, dy) where dx, dy are fractions
    dx, dy = d
    d_square = plus(mul(dx,dx),mul(dy,dy))
    d_numerator = int(sqrt(d_square[0]))
    d_denominator = int(sqrt(d_square[1]))
    dis = (d_numerator, d_denominator)
    if not equal(d_square,mul(dis, dis)):
        raise IrrationalError()
    return dis

#sqrt for big int
def mysqrt(sq):
    lower = 1
    upper = sq
    while upper - lower > 100:
        middle = (upper+lower)//2
        if middle*middle < sq:
            lower = middle
        else:
            upper = middle
    for i in range(lower, upper + 1):
        if i*i == sq:
            return i

def is_squarenumber(n):
    n_root = mysqrt(n)
    if n_root is None:
        return False
    return n_root*n_root == n

def get_pythagoras(p1, p2):
    p_dif = subpos(p2, p1)
    dx = p_dif[0]
    dy = p_dif[1]
    if dx[0]*dx[1]*dy[0]*dy[1] == 0:
        return (1,0,1)
    n_lcm = lcm(dx[1], dy[1])
    a = int(Fraction(dx[0],dx[1])*n_lcm)
    b = int(Fraction(dy[0],dy[1])*n_lcm)
    c_square = a*a + b*b
    if is_squarenumber(c_square):
        return (a, b, mysqrt(c_square))
    return None

def normalize_pythagoras(tup):
    a, b, c = tup
    if a < 0:
        return (b, -a, c)
    return tup

def estimate_pythagoras(lst_edge):
    if len(lst_edge)==0:
        return None
    lst_p = [get_pythagoras(p1, p2) for p1, p2 in lst_edge if get_pythagoras(p1, p2)]
    lst_p = [normalize_pythagoras(pythagoras) for pythagoras in lst_p]
    counter = defaultdict(int)
    for pythagoras in lst_p:
        counter[pythagoras] += 1
    max_freq = 0
    max_pythagoras = None
    for key, value in counter.items():
        if value > max_freq:
            max_freq = value
            max_pythagoras = key
    return max_pythagoras

def rotate(p_target, p_anchor, pythagoras):
    cos = (pythagoras[0], pythagoras[2])
    sin = (pythagoras[1], pythagoras[2])
    p_dif = subpos(p_target, p_anchor)
    x_dif_rot = sub(mul(p_dif[0],cos), mul(p_dif[1], sin))
    y_dif_rot = plus(mul(p_dif[1],cos), mul(p_dif[0], sin))
    return pluspos((x_dif_rot, y_dif_rot), p_anchor)

def unrotate(p_target, p_anchor, pythagoras):
    return rotate(p_target, p_anchor, (pythagoras[0], -pythagoras[1], pythagoras[2]))

class RectangleSolver:
    def __init__(self, problem_path):
        self.set_problem(problem_path)

    def set_problem(self, problem_path):
        with open(problem_path) as fp:
            problem = read_problem(fp)
        self.vertices = problem[0][0]
        self.edges = problem[1]
        self.normalize_problem()

    def print_vertices(self):
        print("vertices")
        print(self.vertices)

    def print_edges(self):
        print("edges")
        print(self.edges)

    def normalize_problem(self):
        self.pythagoras = estimate_pythagoras(self.edges)
        self.print_vertices()
        self.print_edges()
        print("Normalizing begins")
        p_anchor = self.vertices[0]
        self.unrotate_all_vertices(p_anchor, self.pythagoras)
        self.origin_x = self.min_x()
        self.origin_y= self.min_y()
        print(self.pythagoras)
        print(self.origin_x,self.origin_y)
        self.transfer_all_vertices(((-self.origin_x[0],self.origin_x[1]), (-self.origin_y[0],self.origin_y[1])))
        self.unrotate_all_edges(p_anchor, self.pythagoras)
        self.transfer_all_edges(((-self.origin_x[0],self.origin_x[1]), (-self.origin_y[0],self.origin_y[1])))
        self.print_vertices()
        self.print_edges()

    def solve(self):
        max_x = self.max_x()
        max_y = self.max_y()
        print(max_x, max_y)
        points_x = self.how_to_fold(max_x)
        points_y = self.how_to_fold(max_y)
        print("points_x")
        print(points_x)
        print("points_y")
        print(points_y)
        commands_x = [((r, (1,1)), (r, (0,1))) for r in points_x]
        commands_y = [(((0,1), r), (self.min_rational([max_x, (1,1)]), r)) for r in points_y]
        self.commands = commands_x + commands_y
        print("commands")

        # fold
        init_solution_positions = [p00, p10, p11, p01]
        init_result_positions = [p00, p10, p11, p01]
        init_facets = [[0, 1, 2, 3]]
        init = {'sol_positions': init_solution_positions,
                'res_positions': init_result_positions,
                'facets': init_facets}

        state = init
        for command in self.commands:
            print()
            print("=======")
            print(command)
            state = fold(command, state)
        res_lst = state["res_positions"]
        state["res_positions"] = self.unnormalize(res_lst)
        self.print_commands()
        return origami_to_solution(state)

    def how_to_fold(self, r):
        if le((1,1), r):
            return []
        count = 0
        while lt(r, (1,2)):
            count += 1
            r = plus(r, r)
            print(r)
        fold_points = [r]
        for i in range(0, count):
            r = div(r, (2,1))
            fold_points.append(r)
        return fold_points

    def max_rational(self, lst):
        max_r = max([Fraction(r[0], r[1]) for r in lst])
        return (max_r.numerator, max_r.denominator)

    def min_rational(self, lst):
        min_r = min([Fraction(r[0], r[1]) for r in lst])
        return (min_r.numerator, min_r.denominator)

    def min_x(self):
        print("min_x")
        print(self.vertices)
        return self.min_rational([p[0] for p in self.vertices])

    def max_x(self):
        return self.max_rational([p[0] for p in self.vertices])

    def min_y(self):
        print("min_y")
        print(self.vertices)
        return self.min_rational([p[1] for p in self.vertices])

    def max_y(self):
        return self.max_rational([p[1] for p in self.vertices])

    def rotate_all_vertices(self, p_anchor, pythagoras):
        self.vertices = [rotate(p_target, p_anchor, pythagoras) for p_target in self.vertices]

    def unrotate_all_vertices(self, p_anchor, pythagoras):
        self.vertices = [unrotate(p_target, p_anchor, pythagoras) for p_target in self.vertices]

    def transfer_all_vertices(self, vector):
        self.vertices = [pluspos(p_target, vector) for p_target in self.vertices]

    def rotate_all_edges(self, p_anchor, pythagoras):
        new_edges = []
        for edge in self.edges:
            v1 = edge[0]
            v2 = edge[1]
            new_edges.append((rotate(v1, p_anchor, pythagoras), rotate(v2, p_anchor, pythagoras)))
        self.edges = new_edges

    def unrotate_all_edges(self, p_anchor, pythagoras):
        new_edges = []
        for edge in self.edges:
            v1 = edge[0]
            v2 = edge[1]
            new_edges.append((unrotate(v1, p_anchor, pythagoras), unrotate(v2, p_anchor, pythagoras)))
        self.edges = new_edges

    def transfer_all_edges(self, vector):
        new_edges = []
        for edge in self.edges:
            v1 = edge[0]
            v2 = edge[1]
            new_edges.append((pluspos(v1, vector), pluspos(v2, vector)))
        self.edges = new_edges

    def unnormalize(self, p_lst):
        origin = (self.origin_x, self.origin_y)
        output = [pluspos(rotate(p, ((0,1), (0,1)), self.pythagoras), origin) for p in p_lst]
        print(output)
        return output

    def print_commands(self):
        print()
        print("Applied commands are")
        for i, command in enumerate(self.commands):
            print(str(i)+"th command is " + str(command))

if __name__ == '__main__':
    id = sys.argv[1]
    problem_path = "problems/"+id
    rs = RectangleSolver(problem_path)
    result = rs.solve()
    print()
    print(result)