from util.common import *
from split_problem import *
import sys

if __name__ == '__main__':
    with open(sys.argv[1]) as fp:
        polygons, segments = read_problem(fp)
    cross_points, mini_segments = segments_to_mini_segments(segments)
    print('cross_points:', list(map(point_to_str, cross_points)))
    print('mini_segments:')
    for i, ms in enumerate(mini_segments):
          print('ms %d: %s' % (i, ms))
