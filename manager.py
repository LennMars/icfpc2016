import sys,io
import csv
import API.request_sender
import argparse
import time
import json
import os.path
import matplotlib.pyplot as pyplot
#execute in project root directory

snapshot=None

def update_snapshot_data(args):
    print('get latest hash')
    latest_hash = contest_status_snapshot()[-1]
    print(latest_hash)
    time.sleep(1)
    print('get snapshot')
    latest = get_blob(latest_hash['snapshot_hash']).json()

    with open('data/snapshot-latest.txt','w') as file:
       json.dump(latest,file,indent=4)

#    print(latest['problems'])


def contest_status_snapshot():
    r_sender = API.request_sender.RequestSender()
    resp = r_sender.contest_status()

    if(resp.status_code == 200):
        content = resp.json()
        snapshots = content['snapshots']
        return snapshots
    else:
        print("error")
        print(resp.json())
        return None

def get_blob(hash):
    r_sender = API.request_sender.RequestSender()
    resp = r_sender.blob_lookup(hash)

    if(resp.status_code == 200):
        return resp

    else:
        print("error")
        print(resp)

def get_blob_command(args):
    blob = get_blob(args.hash)
    print(blob.content.decode('utf-8'))

def _get_local_snapshot():

    with open('data/snapshot-latest.txt','r') as file:
        snapshot = json.load(file)
   # print('snapshot_time %s' % snapshot['snapshot_time'])

    return snapshot

def latest_problems(args):
    snapshot= _get_local_snapshot()
    problems = snapshot['problems']
    problem_path = 'problems'
    api_use = 0
    for problem in problems:
        problem_id = problem['problem_id']
        if(os.path.exists('%s/%s'%(problem_path,problem_id))):
            print('%s exist'%problem_id)
            continue

        if(api_use > 900):
            print('api limit')
            exit()
        time.sleep(1)
        problem_spec = get_blob(problem['problem_spec_hash'])
        api_use += 1
        if(problem_spec.status_code == 200):
           # print(problem_spec.content.decode('utf-8'))
            print('good')
            print('dl preblem %s' % problem_id)
        else:
            print('api error')
            print(problem_spec)

        p_file = '%s/%s'%(problem_path,problem_id)
        print('finish')
        f=open(p_file,'w')
        f.write(problem_spec.content.decode('utf-8'))
        f.close()

#muzui node botsu
def latest_team_solution_status(args):
    snapshot = _get_local_snapshot()
    print (snapshot.keys())
    return
    content = get_blob(snapshot['snapshot_hash']).json()
    print(content.keys())

def probrem_submit(args):

    r_sender = API.request_sender.RequestSender()
    resp = r_sender.problem_submission(solution_spec=args.solution_spec,publish_time=args.publish_time)

    if(resp.status_code == 200):
        content = resp.json()
        print(content)
#        with open('history/problem_history.csv',newline='') as historyfile:
  #          history = csv.reader(historyfile,)
    else:
        print('error!')
        print(resp.json())

def solution_submit(args):
    r_sender = API.request_sender.RequestSender()
    resp = r_sender.solution_submission(probrem_id=args.problem_id,solution_spec=args.solution_spec)

    if (resp.status_code == 200):
        content = resp.json()
        print(content)
        #        with open('history/problem_history.csv',newline='') as historyfile:
        #          history = csv.reader(historyfile,)
    else:
        print('error!')
        print(resp.json())

def problem_status(problem_id,snapshot):
    if(snapshot is None):
        snapshot = _get_local_snapshot()
    problems = snapshot['problems']
    for problem in problems:
        if(problem['problem_id'] == problem_id):
            return problem,snapshot
    return None,snapshot

def make_plot(args):
    log_file=args.log_file
    problem_size_list=[]
    resemblance_list=[]
    print('make plot')

    with open(log_file,newline='') as csv_file:
        reader = csv.reader(csv_file,delimiter=',')
        snapshot = None

        for row in reader:
            if(row[0] == 'id'):
                continue
            problem,snapshot =  problem_status(int(row[0]),snapshot)
            if(problem is None):
                continue
            resemblance = row[1]
            problem_size = problem['problem_size']
            resemblance_list.append(resemblance)
            problem_size_list.append(problem_size)
    print('start plot')
    pyplot.grid = True
    pyplot.plot(problem_size_list,resemblance_list,'o')
    pyplot.show()

def make_miss_list(args):
    log_file = args.log_file
    miss_list_file='miss_list.txt'
    snapshot = _get_local_snapshot()
    problems = snapshot['problems']
    id_list = []
    for problem in problems:
        id_list.append(problem['problem_id'])

    with open(log_file, newline='') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        for row in reader:
            if(row[0]== 'id'):
                continue
            id_list.remove(int(row[0]))
    print(id_list)

def world_hard_problem(args):
    snapshot = _get_local_snapshot()
    problems = snapshot['problems']
    problem_list =[]
    for problem in problems:
        ranking = problem['ranking']
        if(len(ranking) == 0):
            problem_list.append('%s never'%problem['problem_id'])
        else:
            perfect = 0
            not_perfect  = 0
            max = 0
            for r in ranking:
                resemblance = r['resemblance']
                if(resemblance == 1):
                    perfect += 1
                else:
                    not_perfect += 1
                    if(max < resemblance):
                        max = resemblance
            if(not_perfect == 0):
                continue
            line = '%s perfect:%s not_perfect:%s max%s'%(problem['problem_id'],perfect,not_perfect,max)
            problem_list.append(line)
    file = 'hard_problem.txt'
    with open(file,'w'):
        pass
    with open(file,'w',encoding='utf-8') as f:
        for hard in problem_list:
            f.write(hard)
            f.write('\n')

def default_func(args):
    print('see: python manager.py --help')


sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

parser = argparse.ArgumentParser(description='execute in project root directory')
parser.set_defaults(func=default_func,parser=parser)
subparser = parser.add_subparsers(title='commands')

#Blob Lookup,Contest Status Snapshot,Probrem Submission,Solution Submission
#parser_css = subparser.add_parser('css',help='Contest Status Snapshot')
#parser_css.set_defaults(func=contest_status_snapshot)

parser_ls = subparser.add_parser('lp',help='Get latest problem files(use API * problem num)')
parser_ls.set_defaults(func=latest_problems)

parser_probsub = subparser.add_parser('psub',help='probrem submission (use API once)')
parser_probsub.add_argument('solution_spec',action='store',help='file path')
parser_probsub.add_argument('publish_time',action='store',help='1470441600 (2016-08-06 00:00:00 UTC)  , 1470445200 (2016-08-06 01:00:00 UTC) ...  1470603600 (2016-08-07 21:00:00 UTC)')
parser_probsub.set_defaults(func=probrem_submit)

#parser_sol = subparser.add_parser('sol',help='Get latest team solution status (use API once)')
#parser_sol.set_defaults(func=latest_team_solution_status)

parser_blob = subparser.add_parser('blob',help='Get blob json (use API once)')
parser_blob.add_argument('hash',action='store',help='hash of blob')
parser_blob.set_defaults(func=get_blob_command)

parser_solsub = subparser.add_parser('ssub',help='solution submission (use API once)')
parser_solsub.add_argument('problem_id',action='store',help='id number')
parser_solsub.add_argument('solution_spec',action='store',help='file path')
parser_solsub.set_defaults(func=solution_submit)

parser_updatedb = subparser.add_parser('update',help='update data(use API twice)')
parser_updatedb.set_defaults(func=update_snapshot_data)

parser_plot = subparser.add_parser('plot',help='plot solution log')
parser_plot.add_argument('log_file',action='store',help='file path')
parser_plot.set_defaults(func=make_plot)

parser_misslist = subparser.add_parser('miss',help='make miss problem_id list')
parser_misslist.add_argument('log_file',action='store',help='file path')
parser_misslist.set_defaults(func=make_miss_list)

parser_hard = subparser.add_parser('hard',help='get hard problems')
parser_hard.set_defaults(func=world_hard_problem)

args = parser.parse_args()
args.func(args)
