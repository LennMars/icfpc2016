from util.common import *
from plot_problem import *
from fold import *

init_solution_positions = [p00, p10, p11, p01]
init_result_positions = [p00, p10, p11, p01]
init_facets = [[0, 1, 2, 3]]
init = {'sol_positions': init_solution_positions,
        'res_positions': init_result_positions,
        'facets': init_facets}

if __name__ == '__main__':
    p_05_00 = ((1, 2), (0, 1))
    p_05_10 = ((1, 2), (1, 1))
    origami1 = fold((p_05_00, p_05_10), init)

    p_05_025 = ((1, 2), (1, 4))
    p_10_075 = ((1, 1), (3, 4))
    origami2 = fold((p_05_025, p_10_075), origami1)

    pa = ((7, 8), (5, 8))
    pb = ((5, 4), (5, 8))
    origami3 = fold((pa, pb), origami2)

    problem = origami_to_problem(origami3)
    with open('test2.origami', 'w') as fp:
        print_problem(problem, fp)

    exit()
