from util.common import *
from plot_problem import *
from fold import *

init_solution_positions = [p00, p10, p11, p01]
init_result_positions = [p00, p10, p11, p01]
init_facets = [[0, 1, 2, 3]]
init = {'sol_positions': init_solution_positions,
        'res_positions': init_result_positions,
        'facets': init_facets}

if __name__ == '__main__':
    origami1 = fold((p01, p10), init)

    print('origami1 facets:', origami1['facets'])

    p_00_05 = ((0, 1), (1, 2))
    p_05_05 = ((1, 2), (1, 2))
    origami2 = fold((p_00_05, p_05_05), origami1)
    problem = origami_to_problem(origami2)
    with open('test.origami', 'w') as fp:
        print_problem(problem, fp)

    exit()